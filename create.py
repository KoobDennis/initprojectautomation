import sys
import os
import gitlab
from gitlab.v4.objects import projects

# Setup
projectname = str(sys.argv[1])
projectvisibility = str(sys.argv[2])
projectstruct = str(sys.argv[3])
projectpath = os.environ.get('projectpath')
gtoken = os.environ.get('gtoken')
_projectdir = projectpath + '\\' + projectname

# Create gitlab repo
gl = gitlab.Gitlab('https://gitlab.com', private_token=f'{gtoken}')
gl.auth()
glUsername = gl.user.username
glProject = gl.projects.create({'name': projectname, 'visibility' : projectvisibility})

# Commands
commands = [
    f'echo #{glProject.name} >> README.md',
    'git init',
    f'git remote add origin https://gitlab.com/{glUsername}/{projectname}',
    f'git add .',
    f'git commit -am "init commit"',
    f'git push -u origin master'
]

# Create basic folder structure
os.mkdir(_projectdir)
os.chdir(_projectdir)
if(projectstruct.upper() == "MVC"):
    os.mkdir('src')
    os.chdir('src')
    os.mkdir('model')
    os.mkdir('view')
    os.mkdir('controller')
    os.chdir(_projectdir)

# execute all commands
for c in commands:
    os.system(c)

print(f'Your project {projectname} ist created and the init commit to your gitlab was made')
print(f'Check the create repo https://gitlab.com/{glUsername}/{projectname}')

# open vs code int the project folder
os.system('code .')
