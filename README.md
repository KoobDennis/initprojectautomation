# Project automation
A automated workflow to create new projects by a single command. The Project folder is created as well as a gitlab repository with the first initial commit. No Need to go through all the same commands every time you start a new project.

# Setup 
1. Edit the `"setup.bat"`
```batch
setx gtoken <TOKEN>
setx projectpath <BASEPROJECTPATH>
```
   - Insert a Gitlab token, so the script can authorize you as user. 
   - Enter a base project path. 
2. Execute the batch file to set enviornment variables for the token and project path. 
3. Install the requierments (requirements.txt)
```python
pip install -r requirements.txt
```
4. Add the project folder to the PATH environment variable

Now the Script is ready to use

# Usage
open a new terminal an enter:
```
create <projectname>
```
deafult is a public repository
```
create <projectname> -p
```
creates a private repo
```
create <projectname> -s mvc
```
creates a project folder with following structur:
```
project
--src
----model
----view
----controller
```