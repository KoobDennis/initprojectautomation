@echo off

set projectname=%1
cd /d %~dp0

If "%2" == "-s" (
    set structur=%3
)

If "%3" == "-s" (
    set structur=%4
)

If "%1"=="" (
    echo "ERROR: Please enter a projectname"
) else ( 
    If "%2" == "-p" (
        python create.py %projectname% private %structur%
    ) else (
        python create.py %projectname% public %structur%
    )
)